package config

import (
	"gopkg.in/ini.v1"
)

var Config *ini.File
var EnvConfig map[string]string = map[string]string{}
func InitConfig()  {
	configPATH := "./.env"
	cfg , err := ini.Load(configPATH)
	if err != nil {
		panic(err)
	}
	Config = cfg
}
