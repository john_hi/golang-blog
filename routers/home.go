package routers

import (
	"blog/config"
	"blog/controllers"
	"blog/middleware"
	"github.com/gin-gonic/gin"
)

func Home(r *gin.Engine) {
	appType, ok := config.EnvConfig["appType"]
	if !ok {
		panic("未设置app类型")
	}

	home := r.Group("/")
	{
		// 首页
		home.GET("/", controllers.Index)

		if appType == "admin" {
			article := home.Group("/article", middleware.Authorization)
			{
				// 创建文章
				article.GET("/user", controllers.UserArticleList)
				article.GET("/create", controllers.CreateArticle)
				article.POST("/create", controllers.SaveArticle)
				article.GET("/edit/:id", controllers.EditArticle)
				article.GET("/delete/:id", controllers.DelArticle)
			}
		}

		// 个人中心
		if appType == "admin" {
			user := home.Group("/user", middleware.Authorization)
			{
				user.GET("/update_pwd", controllers.UpdatePwd)
				user.POST("/update_pwd", controllers.DoUpdatePwd)
			}
		}

		// 文章详情
		home.GET("/detail/:id", controllers.Detail)
		home.GET("/about", controllers.About)


		// 标签页面
		tag := home.Group("/tags")
		{
			tag.GET("/", controllers.TagIndex)
			tag.GET("/title/:name", controllers.GetArticleByTagName)
			tag.GET("/ajax/list", controllers.AjaxTags)
		}

		home.GET("/archives", controllers.Archives)

		if appType == "admin" {
			// 注册
			home.GET("/join", controllers.Register)
			home.POST("/join", controllers.DoRegister)

			// sign in
			home.GET("/login", controllers.Login)
			home.POST("/login", controllers.DoLogin)
			home.GET("/logout", controllers.Logout)
		}
	}
}
