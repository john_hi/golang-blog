package controllers

import (
	"blog/bootstrap/driver"
	"blog/config"
	"blog/modules"
	"github.com/gin-gonic/gin"
	"net/http"
)

// Index 项目首页
func Index(c *gin.Context) {
	var articles []modules.Article

	dbQuery := driver.Db.Table("articles").Order("created_at desc")
	paginate, err := (&modules.Pagination{}).Paginate(c, *dbQuery, &articles)

	if err != nil {
		panic(err)
	}

	auth := Auth{}.GetAuth(c)
	header := Header{Title:""}
	appType, _ := config.EnvConfig["appType"]
	data := struct {
		Paginate modules.Pagination
		Auth
		Header
		AppType string
	}{
		*paginate, auth,header,appType,
	}

	c.HTML(http.StatusOK, "index", data)
}
