package main

import (
	"blog/bootstrap"
	"blog/bootstrap/driver"
	"blog/config"
	"fmt"
	"github.com/gin-gonic/gin"
	"gopkg.in/ini.v1"
	"log"
	"strings"
)

var app *gin.Engine

// set app run mode
var appEnv map[string]string

func init() {
	appEnv = make(map[string]string)
	appEnv["debug"] = gin.DebugMode
	appEnv["test"] = gin.TestMode
	appEnv["release"] = gin.ReleaseMode
}

func main() {
	fmt.Println("set env run mode")
	config.InitConfig()
	env := config.Config.Section("env")
	mode, err := env.GetKey("Mode")
	if err != nil {
		panic(err)
	}
	// get config port
	port := getPort(env)
	gin.SetMode(appEnv[strings.ToLower(fmt.Sprintf("%s", mode))])

	fmt.Println("set database driver connect")
	// register database connect
	driver.InitConn("mysql")
	// 启用控制台日志颜色
	gin.ForceConsoleColor()

	fmt.Println("set application router")
	// set application router
	app = bootstrap.Init()

	fmt.Printf("The application run at :%s", port)
	log.Fatal(app.Run(":" + port))
}

func getPort(env *ini.Section) string {
	at, err := env.GetKey("AppType")
	if err != nil {
		panic(err)
	}
	appType := fmt.Sprintf("%s", at)

	var port *ini.Key
	if appType == "web" {
		port, err = env.GetKey("WebPort")
	} else {
		port, err = env.GetKey("AdminPort")
	}

	if err != nil {
		panic(err)
	}

	config.EnvConfig["appType"] = appType
	config.EnvConfig["port"] = fmt.Sprintf("%s", port)

	return fmt.Sprintf("%s", port)
}